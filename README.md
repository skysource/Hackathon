## **Hackathon--小程序社区应用**
建设兴趣社区，实现更多不可能

### 体验一下
待补充...

### 运行

建议使用VSCode+wept开发微信小程序，VSCode安装JWSnippets-For-VSCode就可以将wxml、wxss解析成html和css，而wept支持使用chrome来调试微信小程序。下面讲讲这两种方法的使用：

JWSnippets-For-VSCode

## 下载安装VSCode

下载JWSnippets-For-VSCode

https://github.com/johnwang77/JWSnippets-For-VSCode

安装JWSnippets-For-VSCode
## A. 将json文件放入IDE的sinppets目录：
### VSCode
```
Windows版本：C:\Users\用户名\AppData\Roaming\Code\User\snippets
Mac版本：/Users/用户名/Library/Application Support/Code/User/snippets
```

### Wing
```
Windows版本：C:\Users\用户名\AppData\Roaming\EgretWing\User\snippets
Mac版本：/Users/用户名/Library/Application Support/EgretWing/User/snippets
如果找不到上述目录，可以打开IDE菜单->首选项->用户代码片段，分别选择Javasript、HTML(或者WXML)，将下载的文件代码手动copy进去，保存即可。
```

## B. 打开IDE菜单->首选项->用户设置，在settings.json加入以下代码：

### VSCode：

[AppleScript]
"files.associations": { "*.wxml": "html", "*.wxss": "css"}

### Wing：
[AppleScript]
"files.associations": { "*.wxss": "css"}

## C. 为防止和其他命令冲突，本sinppet触发命令以'jw'开头。 如有疑问，可参照johnwang77大神的github
https://github.com/johnwang77/JWSnippets-For-VSCode

## WEPT

WEPT 是一个微信小程序实时开发环境，它的目标是为小程序开发提供高效、稳定、友好、无限制的运行环境。项目后台使用 node 提供服务完全动态生成小程序，前端实现了 view 层、service 层和控制层之间的相关通讯逻辑。支持 Mac, Window 以及 Linux

### 主要特性:
```
支持 wxml, wxss, javascript 和 json 保存后热更新
支持系统 notification 更早提示构建和请求错误
使用后台转发 XMLHttpRequest 请求，无需配置 CORS, 可 配置代理
支持 所有小程序公开 API
可使用 Chrome 移动页面调试，可在移动端体验
支持 appData 和 storage 面板，需下载 Chrome 插件
https://chrome.google.com/webstore/detail/wechat-devtools-extension/cmpjfobofbhbghjodehbohchlghacmll
```
### **运行**
1.安装nodejs
https://nodejs.org/dist/v10.15.0/node-v10.15.0-x64.msi

#### 使用`wept`启动小程序
```
npm i wept -g
cd 微信小程序所在目录
wept -p 3100
```
启动之后直接在浏览器里打开`localhost:3100`并启用手机调试模式就好了。

#### 启动后台接口
##### mongodb初始化
```
1.下载mongodb数据库程序
http://downloads.mongodb.org/win32/mongodb-win32-x86_64-3.4.17.zip
2.配置mongdb环境变量
3.创建数据库
  mongo
> use myapp
> show dbs
admin  0.000GB
local  0.000GB
> db.runoob.insert({"name":"hello world"})
WriteResult({ "nInserted" : 1 })
> show dbs
admin  0.000GB
local  0.000GB
myapp  0.000GB
```
##### redis安装
1.下载redis
https://github.com/MicrosoftArchive/redis/releases
https://pan.baidu.com/s/1UGFjaJAlU3LLohGgck1Qmw

##### 启动mongodb
```
mongod --config=E:\mongod_install\bin\mongodb.conf
mongodb.conf内容如下
dbpath = F:\Data
```
##### 启动redis
redis-server.exe redis.windows.conf

##### 启动接口
1.安装项目依赖
```
cd api
nmp install
node .
```

### **目录说明**
```
api --- 提供后台接口
  |-common --- loopback的公共模型
  |-server --- loopback的服务器模型
    |- boot --- 初始化执行脚本
    |- modle --- 所有定义的模型目录
    |- datasources.json --- 数据源定义文件
      |- middleware.json --- 中间件配置文件
      |- modle-config.json --- 模型定义文件
      |- server.js --- 主程序
reptile --- 所有的爬虫目录
  |- connectDB --- 连接数据库，操作数据库方法类
  |- tools --- 实用方法类
  |- networkReptile.js --- 爬虫主程序
  |- config.js --- 爬虫配置js
weixin --- 微信小程序目录
  |- assets --- 静态资源文件
  |- datas --- 静态数据
  |- images --- 图片资源文件
  |- page --- 所有微信小程序的页面
  |- util --- 工具类
  |- app.js --- 微信小程序入口文件
```
### **项目截图**
带补充...
